
Exemplo de utilizacao das classes para ler ficheiros WARC/WET no Hadoop.
Deve funcionar em Hadoop versao 1 e versao 2, em modo Standalone (sequencial) e
em modo distribuido (mesmo na Amazon EMR).

Note que deve ter de adaptar a chamada do comando hadoop abaixo
para indicar o pathname correcto (ou então instalar a respectiva
 directoria bin no PATH).

Compilar:
	javac -cp `hadoop classpath`:. TopFiveSiteCount.java edu/cmu/lemurproject/*java
	javac -cp `hadoop classpath`:. TopFiveSiteCount.java edu/cmu/lemurproject/*java datatypes/*java

Jar:
	jar cvf topfivesitecount.jar *.class edu
	jar cvf topfivesitecount.jar *.class edu datatypes

executar com Hadoop (exemplo):
	hadoop jar topfivesitecount.jar TopFiveSiteCount input output

	time hadoop jar topfivesitecount.jar TopFiveSiteCount input/CC-MAIN-20140707234000-00000-ip-10-180-212-248.ec2.internal.warc.wet output-top5-bigfile

Lembre-se que a directoria input tem os ficheiros a processar e a output nao
pode existir e ficara' no fim com o resultado. Estas directorias podem ser
locais (no modo Standalone) ou serem referencias para HDFS ou buckets S3.
