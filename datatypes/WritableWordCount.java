package datatypes;
/**
* This class provides a data type to be used in the mapreduce framework to
* encapsulate the data retrieved from the input. 
* It stores the word (a string) , and the number of occurrences, the count (an integer).
*
* @author Luis Silva, nr 44890
* @author Ricardo Gaspar, nr 42038
* 
*/

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.Writable;

public class WritableWordCount implements Writable {
  public String word;
  public int count;

  public WritableWordCount(String word, int count) {
    this.word = word;
    this.count = count;

  }

  public WritableWordCount() {
    this("", 0);
  }

  public void write(DataOutput out) throws IOException {
    out.writeUTF(word);
    out.writeInt(count);
  }

  public void readFields(DataInput in) throws IOException {
    word = in.readUTF();
    count = in.readInt();
  }

  public String toString() {
    return word + ", " + count;
  }

  public String getWord(){
    return word;
  }

  public int getCount(){
    return count;
  }

}