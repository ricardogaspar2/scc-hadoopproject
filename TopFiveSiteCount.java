
				/**
				 *  TopFiveSiteCount.java - counts the five most common words of each site (top 5).
				 *  usage example of edu.cmu.lemurproject.* InputFormat and RecordReader
				 *  lmt.silva@campus.fct.unl.pt (44890), rf.gaspar@campus.fct.unl.pt (42038) - SCC - 2015/2016
				 *  based on public WordCount examples
				 */

				import java.io.IOException;
				import java.util.*;

				import org.apache.hadoop.fs.Path;
				import org.apache.hadoop.conf.*;
				import org.apache.hadoop.io.*;
				import org.apache.hadoop.mapreduce.*;
				import org.apache.hadoop.mapreduce.lib.output.*;
				import org.apache.hadoop.mapreduce.lib.input.*;
				import org.apache.hadoop.util.*;

				import edu.cmu.lemurproject.*;
				import java.net.URL;
				import datatypes.WritableWordCount;

				import java.util.HashMap;
				import java.util.TreeMap;
				import java.util.Map.Entry;

				/**
				 * @author Luis Silva, nr 44890
				 * @author Ricardo Gaspar, nr 42038
				 *
				 */	
				public class TopFiveSiteCount {
					
					    public static class MyMap extends Mapper<LongWritable, WritableWarcRecord, Text, WritableWordCount> {
					      // Minimum Size of the words to consider
					      private static final int WORD_MIN_SIZE = 4;
					      private Text website = new Text();

								protected void setup( Context cont ) {
									System.err.println(">>>Processing>>> "+((FileSplit)cont.getInputSplit()).getPath().toString() );

								}
					
					      public void map(LongWritable key, WritableWarcRecord value, Context cont )
																			throws IOException, InterruptedException {
									WarcRecord val = value.getRecord();


									String url = val.getHeaderMetadataItem( "WARC-Target-URI" );
									String content = val.getContentUTF8();
									String[] words = parseContent(content, WORD_MIN_SIZE);

									try {
									website.set(new URL(url).getHost());
									for(String word: words){
										// to remove the remainnig strings that couldn't be parsed
										if(!word.equalsIgnoreCase("") && word.length() >= WORD_MIN_SIZE)
											// cont.write(website, new Text(word.toLowerCase()));
											cont.write(website, new WritableWordCount(word.toLowerCase(),1));
									}

									} catch ( Exception e ) {
										e.printStackTrace();
									}
					      }

					      private String[] parseContent(String content, int wordMinSize){

					      	//Split by size, and consider latin alphabet, but it cannot remove all empty strings successfuly
					      	String[] words = content.split("(\\b\\w{1,"+ (wordMinSize-1) +"}\\b)|([^a-zA-Z])");

					      	return words;
					      }
					    }
					
					    public static class MyReduce extends Reducer<Text, WritableWordCount, Text, WritableWordCount> {
							
					     	public void reduce(Text key, Iterable<WritableWordCount> values, Context cont)
																			throws IOException, InterruptedException {
							
							
							HashMap<String, Integer> wordCountTable = new HashMap<String, Integer>();

							for ( WritableWordCount val : values ) {
								String word = val.getWord();
								Integer prevCount = wordCountTable.get(word);
								if (prevCount != null) {
									wordCountTable.put(word, prevCount + val.getCount());
								}else {
									//value that may come from the combiner
									wordCountTable.put(word, val.getCount());
								}
					        }

					        TreeMap<String, Integer> wordCountTableSorted = SortedMapByValue(wordCountTable);
					        String topFive = "";
						        for (int i=0; i<5 ; i++) {
						        	Entry<String, Integer> entry = wordCountTableSorted.pollFirstEntry();
						        	if(entry != null){
						        		try{
											cont.write(key, new WritableWordCount(entry.getKey(), entry.getValue()));
						        		} catch( Exception e){
						        		throw new NullPointerException("*** ERROR: entry is not null. entry key = "+ ((entry.getKey()!=null) ? ""+entry.getKey():"null")+ " entry value ="+ ((entry.getValue()!= null) ? ""+entry.getValue():"null") );
						        		}
						        	}else
						        		break;
					        }
					      }

							private static TreeMap<String, Integer> SortedMapByValue(HashMap<String, Integer> map) {
								ValueComparator vc =  new ValueComparator(map);
								TreeMap<String,Integer> sortedMap = new TreeMap<String,Integer>(vc);
								sortedMap.putAll(map);
							return sortedMap;
							}
					}

					public static class MyCombiner extends Reducer<Text, WritableWordCount, Text, WritableWordCount> {
							
					     	public void reduce(Text key, Iterable<WritableWordCount> values, Context cont)
																			throws IOException, InterruptedException {
								
							HashMap<String, Integer> wordCountTable = new HashMap<String, Integer>();

							for ( WritableWordCount val : values ) {
								String word = val.getWord();
								Integer prevCount = wordCountTable.get(word);
								if (prevCount != null) {
									wordCountTable.put(word, prevCount + val.getCount());
								}else {
									//value that may come from the combiner
									wordCountTable.put(word, val.getCount());
								}
					        }

					        for(String strWord : wordCountTable.keySet()){
					        	cont.write(key, new WritableWordCount(strWord, wordCountTable.get(strWord)));
					        }
					    }

					}
					
					    public static void main(String[] args) throws Exception {
					      Job conf = Job.getInstance( new Configuration(), "TopFiveSiteCount" );
					      conf.setJarByClass(TopFiveSiteCount.class);
					
					      conf.setOutputKeyClass(Text.class);
					      conf.setOutputValueClass(WritableWordCount.class);
					
					      conf.setMapperClass(MyMap.class);
					      conf.setCombinerClass(MyCombiner.class);
					      conf.setReducerClass(MyReduce.class);
					
					      conf.setInputFormatClass(WarcFileInputFormat.class);
					      conf.setOutputFormatClass(TextOutputFormat.class);
					
					      FileInputFormat.setInputPaths(conf, new Path(args[0]));
					      FileOutputFormat.setOutputPath(conf, new Path(args[1]));
					
					      conf.waitForCompletion(true); // submit and wait
					    }
					
				}

				class ValueComparator implements Comparator<String> {
				 
				    Map<String, Integer> map;
				 
				    public ValueComparator(Map<String, Integer> base) {
				        this.map = base;
				    }
				 
				    public int compare(String a, String b) {
				        int compare = map.get(a).compareTo(map.get(b));
				        if (compare == 0) 
				          return -1;
				        else 
				          return compare * -1;
				    }
				}


